import { todosDatabase } from '../firebase/index';
import { database } from 'firebase';
export const FETCH_POSTS = 'FETCH_POSTS';

export function getTodos(){
    return dispatch => {
        todosDatabase.on('value', snapshot =>{
            dispatch({
                type: FETCH_POSTS,
                payload: snapshot.val()
            });
        });
    }
}

export function addTodos(values){
    return dispatch => todosDatabase.push(values);
}
export const deleteTodo = (id) => {
  return dispatch => todosDatabase.child(id).remove();
}
