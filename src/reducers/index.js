import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import TodoReducer from './todo-reducer'

const rootReducer = combineReducers({
    form: formReducer,
    todos: TodoReducer
});

export default rootReducer;