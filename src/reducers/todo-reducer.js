import { FETCH_POSTS } from '../actions/todos-actions';

const initialState = {
    todos: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POSTS:
            return action.payload;
        default: return state;
    }
}
