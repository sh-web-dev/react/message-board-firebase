import React, { Component } from 'react';
import './App.css';
import _ from 'lodash';
import { connect } from 'react-redux';
import { getTodos, addTodos,deleteTodo } from './actions/todos-actions';
import { Field, reduxForm, reset } from 'redux-form';

class App extends Component {

  componentWillMount = () => {
    this.props.getTodos();
  }
  

  renderTodos(){
    return _.map(this.props.todos, (todo, key)=>{
      return(
        <div key={key}>
          <h3>{todo.title}</h3>
          <p>{todo.body}</p>
          <button onClick={()=> {this.props.deleteTodo(key)}}>Delete</button>
        </div>
      );
    })
  }

  renderField(field){
    return (
      <input type="text" {...field.input} placeholder={`Please enter a ${field.label}`}/>
    )
  }

  onSubmit(values){
    this.props.addTodos(values).then(this.props.dispatch(reset('NewTodo')));
  }

  render() {
    const {handleSubmit} = this.props;
    return (
      <div>
        <div>
          <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
          <Field
            name="title"
            component={this.renderField}
            label="Title"
            class=""/>
            <Field
            name="body"
            component={this.renderField}
            label="Body"
            class=""/>
            <button type="submit">Add</button>
          </form>
        </div>
        <div>
        <p>Todos</p>
        {this.renderTodos()}
        </div>
      </div>
    );
  }
}

let form = reduxForm({
  form: 'NewTodo'
})(App);

form = connect(state => ({
  todos: state.todos
}), {getTodos, addTodos, deleteTodo})(form);

export default form;
