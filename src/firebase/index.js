import * as firebase from 'firebase'; 

var config = {
    apiKey: "AIzaSyDyNS50I-rg-7R_qtuzGoW_XxI7pLuNIRw",
    authDomain: "react-todo-app-2b86b.firebaseapp.com",
    databaseURL: "https://react-todo-app-2b86b.firebaseio.com",
    projectId: "react-todo-app-2b86b",
    storageBucket: "react-todo-app-2b86b.appspot.com",
    messagingSenderId: "972612220868"
};
firebase.initializeApp(config);

export const todosDatabase = firebase.database().ref('todos/');